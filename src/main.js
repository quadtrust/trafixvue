import Vue from 'vue'
// import App from './App'
import VueRouter from 'vue-router'

import auth from './auth'
import Dashboard from './components/Dashboard.vue'
import Login from './components/Login.vue'
import About from './components/About.vue'
import Transformers from './components/Transformers.vue'

import Alarms from './components/Alarms'
import troubleticket from './components/troubleticket'
import Dga from './components/Dga'
import Profile from './components/Profile'
import Assets from './components/Assets'

Vue.use(VueRouter)

function requireAuth (to, from, next) {
	if (!auth.loggedIn()) {
		next({
			path: '/login',
			query: {
				redirect: to.fullPath
			}
		})
	} else {
		next()
	}
}

const router = new VueRouter({
	mode: 'history',
	base: __dirname,
	routes: [
		{
			path: '/',
			component: Dashboard,
			beforeEnter: requireAuth
		},
		{
			path: '/dgacalculator',
			component: Dga,
			beforeEnter: requireAuth
		},
		{
			path: '/assets',
			component: Assets,
			beforeEnter: requireAuth
		},
		{
			path: '/profile',
			component: Profile,
			beforeEnter: requireAuth
		},
		{
			path: '/troubleticket',
			component: troubleticket,
			beforeEnter: requireAuth
		},
		{
			path: '/alarms',
			component: Alarms,
			beforeEnter: requireAuth
		},
		{
			path: '/about',
			component: About
		},
		{
			path: '/dashboard',
			component: Dashboard,
			beforeEnter: requireAuth
		},
		{
			path: '/login',
			component: Login
		},
		{
			path: '/logout',
			beforeEnter (to, from, next) {
				auth.logout()
				next('/dashboard')
			}
		},
		// All the other standard links are here :
		{
			path: '/transformers',
			component: Transformers,
			beforeEnter: requireAuth
		}

	]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router
  // template: '<App/>',
  // components: { Dashboard }
})
